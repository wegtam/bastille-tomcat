# Tomcat template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[Tomcat](https://tomcat.apache.org/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of Tomcat and Java (see
[Bastillefile](Bastillefile)) will be installed. Other versions are
specified by their prefix e.g. 7, 85, 9 or 10.

You can set options for the JVM via `JAVA_OPTS` argument.

**Please set proper arguments for the usernames and passwords for the tomcat
manager and admin features!** Also you will need to adjust the `context.xml`
configurations for the manager apps to be able to access them from another
host than the one tomcat is running on.

An external directory which is defined via `EXT_DIR` is expected to exist
and will be mounted into the jail to `/srv/tomcat` which is also the
default path for the host directory. The directory will be mounted via
nullfs with write permissions.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-tomcat.git
```

## Usage

Please note that you will have to make some adjustments to the configuration
after you have started the service!

### 1. Install with default settings

```
# bastille template TARGET wegtam/bastille-tomcat
```

### 2. Install with custom settings

```
# bastille template tomcat wegtam/bastille-tomcat --arg VERSION=7 \
  --arg JAVA_VERSION=8 --arg EXT_DIR=/tmp/test-tomcat 
```
